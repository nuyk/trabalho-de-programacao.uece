#Trabalho de Programação orietado a objeto
# agenda de contatos
# Feito por: Hianuy Esperidião De Sousa Pinto




agenda = []           # lista fazia

def pede_nome():        # Função que solicita o nome ex:Pedro
    return(input("Nome: "))

def _telefone():        # Função que solicita o telefone ex:99215857
    return(input("Telefone: "))

def codigo_area():      #Função que solicita o codigo de area ex :85(Código de Fortaleza)
    return(input("Codigo de Area: "))

def _operadora():        #Função que solicita a operadora(OI,TIM,VIVO,CLARO)
    return(input("Operadora: "))

def _email():             #Função que solicita um email ex:joao2345@gmail.com
    return(input("Email:  "))

def _pais():               #Função que solicita o pais de origem do contato ex:CHILE
    return(input("País: "))

def _estado():              #Função que solicita o estado ex: ACRE
    return (input("Estado: "))

def _cidade():              #Função que solicita a cidade ex: Fortaleza
    return(input("Cidade: "))

def _bairro():              #Função que solicita o bairro ex: Centro
    return (input("Bairro: "))

def _residencia():          #Função que solicita a residencia ou localidade pode ser uma RUA ou AVENIDA EX:AVENIDA DUQUE DE CAXIAS
    return(input("Residencia: "))

def _cep():                 #Função que solicita o CEP(Código de Edereçamento Postal) ex:60052002
    return(input("CEP: "))

def _numero():              #Função que solicita o numero da casa ou apartamento ex: apartamento 230
    return(input("Número: "))

def _complemento():         #Função que solicita o complemento(algo que se possa servir de referencia para encontrar o endereço) ex: Proximo ao terminal Antonio Bezerra
    return(input("Complemento: "))

                            # Função que mostra todos os dados do contato.
def listar_dados(nome,telefone,codigo,operadora,email,pais,estado,cidade,bairro,residencia,cep,numero,complemento):
    print(" %s\nnome: %s\nTelefone:  %s\ncodigo:  %s\noperadora: %s\nEmail: %s\npais:  %s\nestado: %s\ncidade: %s\nbairro  %s\nresidencia:  %s\ncep:  %s\nnumero: %s\ncomplemento:   " % (nome,telefone,codigo,operadora,email,pais,estado,cidade,bairro,residencia,cep,numero,complemento))


def pede_nome_arquivo(): #Função que solicita um nome para arquivo,  isso nos auxiliará a guardar os dados quando encerramos o programa
    return(input("Nome do arquivo:  "))


def _pesquisa(nome): # Funçao para pesquisar contatos
    mnome = nome.lower()
    for k, e in enumerate(agenda): # a condição for executa um loop
        if e[0].lower() == mnome:
            return k
    return None           #executado caso a condição if for falsa

def novo(): # Função para adicionar um novo contato a agenda(lista)
    global agenda
    nome = pede_nome()
    telefone = _telefone()
    codigo = codigo_area()
    operadora = _operadora()
    email = _email()
    pais = _pais()
    estado = _estado()
    cidade = _cidade()
    bairro = _bairro()
    residencia = _residencia()
    numero = _numero()
    complemento = _complemento()
    cep = _cep()

    agenda.append([nome,telefone,codigo,operadora,email,pais,estado,cidade,bairro,residencia,numero,complemento,cep])



def apaga(): #Função para apagar o contato
    global agenda                    # Definindo a variavel agenda como global com isso poderei usar essa variavel em toda a função
    nome = pede_nome()               # Entrada de dados
    k = _pesquisa(nome)              # Cria uma variavel k = a função de pesquisa
    if k != None:                    # Determinamos a condição caso k for diferente de nenhum(none) ou seja caso p esteja na agenda
        del agenda[k]                # Apagar o contato caso a condição seja verdadeira
    else:
        print("Nome não encontrado.")# Caso o p nao esteja na agenda

def altera(): #Funçao que altera todos os dados (nome,telefone,codigo,operadora,pais,estado,bairro,residencia,numero,complemento,cep)
     k = _pesquisa(pede_nome())      # A variavel p recebe a função pesquisa que chama a função pedir um nome
     if k != None:                   # Se o p estiver na agenda ou seja o nome
         nome = agenda[k][0]         #Funçao pesquisa esta procurando os dados
         telefone = agenda[k][1]     #Funçao pesquisa esta procurando os dados
         codigo = agenda[k][2]       #Funçao pesquisa esta procurando os dados
         operadora = agenda[k][3]    #Funçao pesquisa esta procurando os dados
         email = agenda[k][4]        #Funçao pesquisa esta procurando os dados
         pais = agenda[k][5]         #Funçao pesquisa esta procurando os dados
         estado = agenda[k][6]       #Funçao pesquisa esta procurando os dados
         cidade = agenda[k][7]       #Funçao pesquisa esta procurando os dados
         bairro = agenda[k][8]       #Funçao pesquisa esta procurando os dados
         residencia = agenda[k][9]   #Funçao pesquisa esta procurando os dados
         numero = agenda[k][10]      #Funçao pesquisa esta procurando os dados
         complemento = agenda[k][11] #Funçao pesquisa esta procurando os dados
         cep = agenda[k][12]         #Funçao pesquisa esta procurando os dados
         print("Encontrado:")        #Contato encontrado
                                     #Os dados precisam ser mostrados na tela para que o usuario possa ver
                                     #Novamente a função listar os dados é chamada para imprimir os dados na tela
         listar_dados(nome,telefone,codigo,operadora,email,pais,estado,cidade,bairro,residencia,numero,complemento,cep)

         nome = pede_nome()          #Colocando novos dados(Nome)
         telefone = _telefone()      #Colocando novos dados
         codigo = codigo_area()      #Colocando novos dados
         operadora = _operadora()    #Colocando novos dados
         email = _email()            #Colocando novos dados
         pais = _pais()              #Colocando novos dados
         estado = _estado()          #Colocando novos dados
         cidade = _cidade()          #Colocando novos dados
         bairro = _bairro()          #Colocando novos dados
         residencia = _residencia()  #Colocando novos dados
         numero = _numero()          #Colocando novos dados
         complemento = _complemento()#Colocando novos dados
         cep = _cep()                #Colocando novos dados
                                     # Armazenando os novos dados na agenda(lista)
         agenda[k] = [nome,telefone,codigo,operadora,email,pais,estado,cidade,bairro,residencia,numero,complemento,cep]
     else:                           #Caso(false)contato não encontrado
         print("Nome não encontrado.")


def lista(): #função que lista todos os contatos na agenda(lista)
    print("\nAgenda\n\n--------")

    for c in agenda:
        print("\n  ", end="")
        listar_dados(c[0]  ,c[1],  c[2], c[3], c[4], c[5], c[6], c[7], c[8], c[9], c[10], c[11], c[12])
    print("--------\n")




def grava(): #função que grava os contatos(escreve o arquivo)
    nome_arquivo = pede_nome_arquivo()# pedimos o nome arquivo que sera criado
    arquivo = open(nome_arquivo, "w", encoding="utf-8") # abrimos o arquivo e  iremos escrever no arquivo
    for c in agenda:
        arquivo.write("%s#%s#%s#%s#%s#%s#%s#%s#%s#%s#%s#%s#%s\n" % (c[0],c[1],c[2],c[3],c[4],c[5],c[6],c[7],c[8],c[9],c[10],c[11],c[12]))# escrevemos os dados armazenados
    arquivo.close() #fechando o arquivo

def recupera(): # função que recupera os dados após sair do programa(lê o arquivo)
        global agenda #atribuimos o valor global a variavel agenda para usarmos na função
        nome_arquivo = pede_nome_arquivo()# pedimos o nome do arquivo para ser lido
        arquivo = open(nome_arquivo, "r", encoding="utf-8") #abrimos o arquivo e iremos lê-lo
        agenda = []
        for l in arquivo.readlines():#A função readlines() retorna cada linha do arquivo como um elemento de uma lista
            nome, telefone,codigo,operadora,email,pais,estado,cidade,bairro,residencia,numero,complemento,cep = l.strip().split("#")

            agenda.append([nome, telefone, codigo, operadora, email, pais, estado, cidade, bairro, residencia, numero,complemento,cep])

        arquivo.close() #fechando o arquivo

def ordena(): #função que ordena os contatos por nome(ordem alfabética)

    fim = len(agenda)
    while fim > 1:
        i = 0
        trocou = False
        while i < (fim - 1):
            if agenda[i] > agenda[i + 1]:
                # Opção: agenda[i], agenda[i+1] = agenda[i+1], agenda[i]
                temp = agenda[i + 1]
                agenda[i + 1] = agenda[i]
                agenda[i] = temp
                trocou = True
            i += 1
        if not trocou:
            break




def valida_faixa_inteiro(pergunta, inicio, fim): #Função para validar numeros inteiros ou tratamento de erro
    while True:                                  #Criando um loop infinito
        try:                                     #Criando um acordo/condição

            valor = int(input(pergunta))         #Entrada de dados
            if inicio <= valor <= fim:           #Determinando a condição
                return (valor)                   #Executando caso for verdadeira

        except ValueError:                      #Executando caso for falsa
            print("Valor inválido, favor digitar entre %d e %d" % (inicio, fim))

            """
            Estou  usando a instrução Try-except, para tratar de um erro,
            Caso o usuário digite um dado diferente do tipo inteiro.ex:(float)
            Vai gerar um erro de valor (ValueError) e irá fazer com que programa
            pare de funcionar e mostre uma mensagem de erro na tela.
            Com  essa instrução crio minha propria mensagem de erro, 
            e assim posso  evitar a parada do  programa.
            """
def menu(): # função para exibir o menu 
     print("""
   1 - Novo 
   2 - Altera
   3 - Apaga
   4 - Lista
   5 - Grava
   6 - Recuperar
   7 - Ordena por nome

   0 - Sai
""")
     print("\nNomes na agenda: %d\n" % len(agenda))
     return valida_faixa_inteiro("Escolha uma opção: ",0,7)#  A mensagem será impressa caso o usario entre com numeros não inteiros.Caso o usuario entre com numeros inteiros mas diferentes de 0-7 o menu sera novamente impresso

while True:
     opção = menu()
     if opção == 0: #Caso o usuario digite (0) o programa fecha
         break
     elif opção == 1:#Caso o usuario digite (1) o programa vai iniciar o procedimento para adicionar o contato na agenda
         novo()
     elif opção == 2:#Caso o usuario digite (2) o programa vai iniciar o procedimento para alterar dados o contato na agenda
         altera()
     elif opção == 3:#Caso o usuario digite (3) o programa vai iniciar o procedimento para apagar um contato na contato na agenda
         apaga()
     elif opção == 4:#Caso o usuario digite (4) o programa vai iniciar o procedimento para listrar todos os contatos o contato na agenda
         lista()
     elif opção == 5:#Caso o usuario digite 5 o programa vai iniciar o procedimento para gravar(escrever no novo arquivo)  o contato na agenda atraves de um arquivo
         grava()
     elif opção == 6:#Caso o usuario digite 6 o programa vai iniciar o procedimento para recuperar(ler o arquivo) o contato na agenda
         recupera()
     elif opção == 7:#Caso o usuario digite 7 o programa vai iniciar o procedimento para ordenar em ordem alfabetica so contatos na agenda
         ordena()