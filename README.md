Usaremos a linguagem python para criarmos uma agenda de contatos, que visa ter os seguites recursos:

Contato deve possuir nome, lista de telefones (com número, código de área e operadora), endereço (com rua, número, complemento, bairro, cep, cidade, estado e país), lista de emails, aniversário (informando quantos dias faltam para a data) e grupo a qual pertence (entre amigos, família e trabalho).
Contatos relativos a trabalho possuem setor, empresa e lista de reuniões marcadas (com data e horário, informando quantos dias faltam para a data).
Contatos relativos a amigos possuem lista de reuniões marcadas (com data e horário, informando quantos dias faltam para a data).
Contatos relativos a família possuem parentesco (avô, avó, pai, mãe, irmão, irmã, primo, prima ou outro).
A agenda deve realizar as seguintes operações:
Adicionar, remover e editar contato, telefone, email e reuniões.
listar todos os contatos e por grupo.
buscar contato por nome, telefone e email.
Salvar os contatos existentes em arquivo e abrir um arquivo com contatos carregando na agenda.